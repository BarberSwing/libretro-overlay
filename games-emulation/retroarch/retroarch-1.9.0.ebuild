# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

MY_PN="RetroArch"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Cross-platform, sophisticated frontend for the libretro API"
HOMEPAGE="http://www.retroarch.com/"
SRC_URI="https://github.com/libretro/RetroArch/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="
	X
	alsa
	+assets
	cheats
	+core-info
	coreaudio
	cpu_flags_x86_sse
	cpu_flags_x86_sse2
	cursors
	ffmpeg
	flac
	glslang
	jack
	+joypad-autoconfig
	libass
	libcaca
	+mmap
	openal
	opengl
	oss
	pulseaudio
	qt5
	+rdb
	sdl
	sdl2
	sixel
	ssl
	systemd
	+threads
	truetype
	udev
	upnp
	usb
	v4l
	vulkan
	wayland
	zlib
"
REQUIRED_USE="sdl2? ( !sdl )"

DEPEND="
	X? (
		x11-libs/libX11
		x11-libs/libXext
		x11-libs/libXxf86vm
		x11-libs/libxcb
	)
	alsa? ( media-libs/alsa-lib )
	ffmpeg? ( virtual/ffmpeg )
	flac? ( media-libs/flac )
	glslang? ( dev-util/glslang )
	jack? ( virtual/jack )
	libass? ( media-libs/libass )
	libcaca? ( media-libs/libcaca )
	openal? ( media-libs/openal )
	opengl? ( virtual/opengl )
	pulseaudio? ( media-sound/pulseaudio )
	qt5? (
		dev-qt/qtconcurrent:5
		dev-qt/qtcore:5
		dev-qt/qtgui:5
		dev-qt/qtnetwork:5
		dev-qt/qtwidgets:5
	)
	sdl? ( media-libs/libsdl )
	sdl2? ( media-libs/libsdl2 )
	sixel? ( media-libs/libsixel )
	ssl? ( net-libs/mbedtls )
	systemd? ( sys-apps/systemd )
	truetype? ( media-libs/freetype )
	udev? ( virtual/udev )
	upnp? ( net-libs/miniupnpc )
	usb? ( virtual/libusb:= )
	v4l? ( media-libs/libv4l )
	vulkan? ( media-libs/vulkan-loader )
	wayland? ( dev-libs/wayland )
	zlib? ( sys-libs/zlib )
"
RDEPEND="${DEPEND}
	assets? ( games-emulation/retroarch-assets )
	cheats? ( games-emulation/libretro-database[cheats] )
	core-info? ( games-emulation/libretro-core-info )
	cursors? ( games-emulation/libretro-database[cursors] )
	joypad-autoconfig? ( games-emulation/retroarch-joypad-autoconfig[X?,sdl2?,udev?] )
	rdb? ( games-emulation/libretro-database[rdb] )
"
BDEPEND=""

S="${WORKDIR}/${MY_P}"

src_prepare() {
	default

	sed -i \
		's/)\( : ;;\)/|--datadir=*|--infodir=*|--libdir=*|--localstatedir=*)\1/g' \
		qb/qb.params.sh || die

	retroarch_cfg() {
		local key="${1}"
		local value="$(printf '%s\n' "${2}" | sed 's/\//\\\//g')"
		sed -i "s/# \(${key} =\).*/\1 ${value}/g" retroarch.cfg || die
	}

	if use assets; then
		retroarch_cfg assets_directory '"/usr/share/libretro/assets"'
	fi

	if use cheats; then
		retroarch_cfg cheat_database_path '"/usr/share/libretro/database/cht"'
	fi

	if use core-info; then
		retroarch_cfg libretro_info_path '"/usr/share/libretro/info"'
	fi

	if use cursors; then
		retroarch_cfg cheat_database_path '"/usr/share/libretro/database/cursor"'
	fi

	if use joypad-autoconfig; then
		retroarch_cfg joypad_autoconfig_dir '"/usr/share/libretro/autoconfig"'
	fi

	if use rdb; then
		retroarch_cfg content_database_path '"/usr/share/libretro/database/rdb"'
	fi

	if use vulkan; then
		retroarch_cfg video_driver '"vulkan"'
	fi
}

src_configure() {
	econf \
		--enable-flac \
		--enable-ssl \
		--enable-zlib \
		$(use_enable X x11) \
		$(use_enable alsa) \
		$(use_enable coreaudio) \
		$(use cpu_flags_x86_sse && use cpu_flags_x86_sse2 && echo --enable-sse) \
		$(use_enable ffmpeg) \
		$(use_enable !flac builtinflac) \
		$(use_enable !glslang builtinglslang) \
		$(use_enable jack) \
		$(use_enable libass ssa) \
		$(use_enable libcaca caca) \
		$(use_enable mmap) \
		$(use_enable openal al) \
		$(use_enable opengl) \
		$(use_enable oss) \
		$(use_enable pulseaudio pulse) \
		$(use_enable qt5 qt) \
		$(use_enable sdl) \
		$(use_enable sdl2) \
		$(use_enable sixel) \
		$(use_enable !ssl builtinmbedtls) \
		$(use_enable systemd) \
		$(use_enable threads) \
		$(use_enable truetype freetype) \
		$(use_enable udev) \
		$(use_enable !upnp builtinminiupnpc) \
		$(use_enable usb libusb) \
		$(use_enable v4l v4l2) \
		$(use_enable vulkan) \
		$(use_enable wayland) \
		$(use_enable !zlib builtinzlib)
}
