# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

commit=07b73973e1becfc120f15ad176926bb582d67157
MY_P="${PN}-${commit}"

DESCRIPTION="Assets needed for RetroArch"
HOMEPAGE="https://github.com/libretro/retroarch-assets"
SRC_URI="https://github.com/libretro/retroarch-assets/archive/${commit}.tar.gz -> ${P}.tar.gz"

LICENSE="CC-BY-4.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="
	glui
	+ozone
	+xmb
"

REQUIRED_USE="ozone? ( xmb )"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${MY_P}"

src_install() {
	insinto /usr/share/libretro/assets

	doins -r rgui

	if use glui; then
		doins -r glui
	fi

	if use ozone; then
		doins -r ozone
	fi

	if use xmb; then
		doins -r xmb
	fi
}
