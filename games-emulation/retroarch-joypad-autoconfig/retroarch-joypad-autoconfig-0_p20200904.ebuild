# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

commit=1a7601ea79ee37583cd2ad01f98ae1ea1f40e812
MY_P="${PN}-${commit}"

DESCRIPTION="RetroArch joypad autoconfig files"
HOMEPAGE="https://github.com/libretro/retroarch-joypad-autoconfig"
SRC_URI="https://github.com/libretro/retroarch-joypad-autoconfig/archive/${commit}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="
	X
	sdl2
	udev
"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${MY_P}"

src_install() {
	insinto /usr/share/libretro/autoconfig

	doins -r linuxraw

	if use X; then
		doins -r x
		doins -r xinput
	fi

	if use sdl2; then
		doins -r sdl2
	fi

	if use udev; then
		doins -r udev
	fi
}
