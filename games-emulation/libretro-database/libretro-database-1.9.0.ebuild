# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Repository containing cheatcode files, content data files, etc"
HOMEPAGE="https://github.com/libretro/libretro-database"
SRC_URI="https://github.com/libretro/libretro-database/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="
	cheats
	cursors
	rdb
"

RDEPEND=""
DEPEND=""
BDEPEND=""

src_install() {
	insinto /usr/share/libretro/database

	if use cheats; then
		doins -r cht
	fi

	if use cursors; then
		doins -r cursors
	fi

	if use rdb; then
		doins -r rdb
	fi
}
